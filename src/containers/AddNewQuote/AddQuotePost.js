import React, {Component} from 'react';
import './AddNewQuote.css'
import axios from "../../axios-blog";

class AddQuotePost extends Component {
       state = {
           title : '' ,
           description : '',
           author : ''
       }

    valueChanged = (event) => {
        const name = event.target.name;
        this.setState({[name]: event.target.value});
    };
    quoteHandler = () => {
        const quote = {title: this.state.title, description: this.state.description, author: this.state.author};
        axios.post('/quotes.json', quote).then(response => {
            this.props.history.push("/all")
        })
    };

    render() {
        return (
            <div className="block-form">
                <span className="span-title">Category</span>
                {/*<input onChange={this.valueChanged} name="title" value={this.state.title} className="form-add" type="text"/>*/}
                <select name="title" id="" className='select-category' onChange={this.valueChanged}>
                    <option value={this.state.title}>Star Wars</option>
                    <option value={this.state.title}>Motivational</option>
                    <option value={this.state.title}>Famous People</option>
                    <option value={this.state.title}>Saying</option>
                    <option value={this.state.title}>Humour</option>
                </select>
                <span className='span-title'>Author</span>
                <input onChange={this.valueChanged} name="author" value={this.state.author} className="add-form" type="text"/>
                <span className='span-title'>Quote text</span>
                <textarea className="description-text-area" type="text" name="description"
                          onChange={this.valueChanged}
                          value={this.state.description}>
                </textarea>
                <button type="button" className="btn-save" onClick={this.quoteHandler}>Submit</button>
            </div>
        );
    }
}

export default AddQuotePost;