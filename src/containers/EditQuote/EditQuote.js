import React, {Component} from 'react';
import AddQuotePost from "../AddNewQuote/AddQuotePost";

class EditQuote extends Component {
    state = {
        quotes: null
    };
    render() {
        let form = <AddQuotePost onClick={this.quoteHandler} posts={this.state.quotes}/>;
        return (
            <div>
                <h3 className="title">Edit Blog </h3>
                {form}
            </div>
        );
    }
}

export default EditQuote;