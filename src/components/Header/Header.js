import React from 'react';
import {NavLink} from "react-router-dom";
import './Header.css';
const Header = () => {
    return (
        <div className="header clearfix">
            <p  className="logo">Quates Central</p>
          <ul className="nav-ul">
              <li><NavLink className="all_link" to="/all">All</NavLink></li>
              <li><NavLink className="submit_link" to="/submit">Submit new quote</NavLink></li>
              <li><NavLink className="link" to="/starWars">Star Wars</NavLink></li>
              <li><NavLink className="link" to="/motivational">Motivational</NavLink></li>
              <li><NavLink className="link" to="/famousPeople">Famous People</NavLink></li>
              <li><NavLink className="link" to="/humour">Humour</NavLink></li>
              <li><NavLink className="link" to="/saying">Saying</NavLink></li>
          </ul>
        </div>
    );
};

export default Header;